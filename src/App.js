import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <section className="page_accueil">
        <ul className="nav">
            <li>
                <a>Web</a>
            </li>
            <li>
                <a>Images</a>
            </li>
            <li>
                <a>Videos</a>
            </li>
            <li>
                <a>News</a>
            </li>
        </ul>
        </section>
      </div>
    );
  }
}

export default App;
